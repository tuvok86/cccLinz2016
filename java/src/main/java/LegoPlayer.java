import cc.catalysts.contest.drones.PlayerFactory;
import cc.catalysts.contest.drones.level.Level;
import cc.catalysts.contest.drones3d.*;
import cc.catalysts.contest.drones3d.scenarios.*;

import java.io.OutputStream;
import java.util.Scanner;

public class LegoPlayer implements PlayerFactory<Simulation3D> {
    LegoPlayer() {
    }

    public Player getPlayer(int l, int t, OutputStream out, Scanner sc) {
        return new Player(this.getLevel(l, t), out, sc);
    }

    private Level<Simulation3D> getLevel(int l, int t) {
        if(l == 1 && t == 1) {
            return (new Scenario11()).initialize();
        } else if(l == 1 && t == 2) {
            return (new Scenario12()).initialize();
        } else if(l == 1 && t == 3) {
            return (new Scenario13()).initialize();
        } else if(l == 2 && t == 1) {
            return (new Scenario21()).initialize();
        } else if(l == 2 && t == 2) {
            return (new Scenario22()).initialize();
        } else if(l == 2 && t == 3) {
            return (new Scenario23()).initialize();
        } else if(l == 3 && t == 1) {
            return (new Scenario31()).initialize();
        } else if(l == 3 && t == 2) {
            return (new Scenario32()).initialize();
        } else if(l == 3 && t == 3) {
            return (new Scenario33()).initialize();
        } else if(l == 4 && t == 1) {
            return (new Scenario41()).initialize();
        } else if(l == 4 && t == 2) {
            return (new Scenario42()).initialize();
        } else if(l == 4 && t == 3) {
            return (new Scenario43()).initialize();
        } else if(l == 5 && t == 1) {
            return (new Scenario51(3600.0D)).initialize();
        } else if(l == 5 && t == 2) {
            return (new Scenario52(7200.0D)).initialize();
        } else if(l == 5 && t == 3) {
            return (new Scenario53(10800.0D)).initialize();
        } else if(l == 6 && t == 1) {
            return (new Scenario61(3600.0D)).initialize();
        } else if(l == 6 && t == 2) {
            return (new Scenario62(7200.0D)).initialize();
        } else if(l == 6 && t == 3) {
            return (new Scenario63(10800.0D)).initialize();
        } else if(l == 7 && t == 1) {
            return (new Scenario71(3600.0D)).initialize();
        } else if(l == 7 && t == 2) {
            return (new Scenario72(7200.0D)).initialize();
        } else if(l == 7 && t == 3) {
            return (new Scenario73(10800.0D)).initialize();
        } else {
            throw new IllegalArgumentException();
        }
    }
}