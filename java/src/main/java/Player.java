import cc.catalysts.contest.drones.AbstractPlayer;
import cc.catalysts.contest.drones.level.Level;
import com.google.common.base.Optional;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * Created by peter5 on 08.04.2016.
 */
public class Player extends AbstractPlayer {
    protected Player(Level level, OutputStream out, Scanner sc) {
        super(level, out, sc);
    }

    @Override
    protected Optional<Boolean> dispatchCommand(String s) throws IOException {
        Scanner scanner = sc;

        System.out.println(scanner);

        return null;
    }
}
