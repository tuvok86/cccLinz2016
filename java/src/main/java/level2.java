import cc.catalysts.contest.drones.Simulation;
import cc.catalysts.contest.drones.level.Level;

import java.io.PrintWriter;

/**
 * Created by peter5 on 08.04.2016.
 */
public class Level2 extends Level<Simulation> {

    public Level2(Simulation simulation) {
        super(simulation);
    }

    @Override
    public boolean success() {
        return true;
    }

    @Override
    public void init(PrintWriter printWriter) {

    }
}
