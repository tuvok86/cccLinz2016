import cc.catalysts.contest.drones.Simulation;
import cc.catalysts.contest.drones.Simulator;
import cc.catalysts.contest.drones.level.Level;
import cc.catalysts.contest.drones3d.Simulation3D;
import cc.catalysts.contest.drones3d.drone.Drone;
import cc.catalysts.contest.drones3d.geometry.Vector2D;
import cc.catalysts.contest.drones3d.map.Terrain;
import cc.catalysts.contest.drones3d.scenarios.Scenario21;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Test
public class ExampleTest {

//    public void testMain() {
//
//        Level2 level2 = new Level2(new LegoSim());
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream("src/test/resources/results-level2.txt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        FileInputStream fis = null;
//        try {
//            fis = new FileInputStream("src/test/resources/input-level2.txt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        Scanner scanner = new Scanner(fis);
//
//        Player p = new Player(level2, fos, scanner);
//
//        try {
//            p.dispatchCommand("STATUS 0");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//



//    LegoPlayer legoPlayer = new LegoPlayer();
//
//    Simulator simulator = new Simulator(legoPlayer);
//
//    String[] args = new String[20];
//    args[0] = "2";
//    args[1] = "1";
//
//    try {
//        simulator.run(args);
//        legoPlayer.getPlayer()
//    } catch (Exception e) {
//        e.printStackTrace();
//    }



//    }

    public void testScenario21() {

        Scenario21 scenario21 = new Scenario21();

        Level<Simulation3D> level = scenario21.initialize();

        Simulation3D simulation = level.getSimulation();


        List<Drone> drones = simulation.getDrones();

        Drone drone0 = drones.get(0);

        drone0.setThrottle(1.0);
        System.out.println(drones.toString());

    }
}
